package id.co.bussan.assigncmo.Model;

import com.google.gson.annotations.SerializedName;


/**
 * Created by 0427701017 on 11/22/2017.
 */

public class GetObjectEmployee {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ObjectEmployee listEmployee;

    @SerializedName("message")
    String message;
}
