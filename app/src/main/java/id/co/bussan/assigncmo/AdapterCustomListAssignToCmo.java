package id.co.bussan.assigncmo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
/**
 * Created by 0426611017 on 11/9/2017.
 */

public class AdapterCustomListAssignToCmo extends BaseAdapter {
    private static ArrayList<ObjectCmo> listCmo;
    private LayoutInflater mInflater;

    public AdapterCustomListAssignToCmo(Context assignToCmoFragment, ArrayList<ObjectCmo> results){
        listCmo = results;
        mInflater = LayoutInflater.from(assignToCmoFragment);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.listview_assign_to_cmo, null);
            holder = new ViewHolder();
            holder.cmoNameTextField = (TextView) convertView.findViewById(R.id.cmo_name);
            holder.cmoCaseTextField = (TextView) convertView.findViewById(R.id.cmo_case);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.cmoNameTextField.setText(listCmo.get(position).getCmoName());
        holder.cmoCaseTextField.setText(listCmo.get(position).getCmoCase());

        return convertView;
    }

    static class ViewHolder{
        TextView cmoNameTextField;
        TextView cmoCaseTextField;
    }

    @Override
    public int getCount() {
        return listCmo.size();
    }

    @Override
    public Object getItem(int arg0) {
        return listCmo.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
}
