package id.co.bussan.assigncmo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by 0426591017 on 11/3/2017.
 */

public class AdapterCustomListPtm extends BaseAdapter {
    private static ArrayList<ObjectPtm> listPtm;
    private LayoutInflater mInflater;

    public AdapterCustomListPtm(Context FragmentListPTM, ArrayList<ObjectPtm> results){
        listPtm = results;
        mInflater = LayoutInflater.from(FragmentListPTM);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        AdapterCustomListPtm.ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.listview_ptm_row, null);
            holder = new AdapterCustomListPtm.ViewHolder();
            holder.ptmDateTextField = (TextView) convertView.findViewById(R.id.ptm_date);
            holder.ptmCustomerNameTextField = (TextView) convertView.findViewById(R.id.ptm_customer_name);
            holder.ptmAddressTextField = (TextView) convertView.findViewById(R.id.ptm_address);
            holder.ptmPhoneNumberTextField = (TextView) convertView.findViewById(R.id.ptm_phone_number);

            convertView.setTag(holder);
        } else {
            holder = (AdapterCustomListPtm.ViewHolder) convertView.getTag();
        }

        holder.ptmDateTextField.setText(listPtm.get(position).getPtmDate());
        holder.ptmCustomerNameTextField.setText(listPtm.get(position).getPtmCustomerName());
        holder.ptmAddressTextField.setText(listPtm.get(position).getPtmAddress());
        holder.ptmPhoneNumberTextField.setText(listPtm.get(position).getPtmPhoneNumber());

        return convertView;
    }

    static class ViewHolder{
        TextView ptmDateTextField;
        TextView ptmCustomerNameTextField;
        TextView ptmAddressTextField;
        TextView ptmPhoneNumberTextField;
    }

    @Override
    public int getCount() {
        return listPtm.size();
    }

    @Override
    public Object getItem(int arg0) {
        return listPtm.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

}
