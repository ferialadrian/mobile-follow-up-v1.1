package id.co.bussan.assigncmo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class FragmentCmoListPtm extends Fragment implements AdapterView.OnItemClickListener {
    private static ArrayList<ObjectPtm> listPtm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_cmo_list_ptm, container, false);

        listPtm = getListPtm();
        ListView lv = (ListView)view.findViewById(R.id.list_view_cmo_ptm);
        lv.setAdapter(new AdapterCustomListPtm(getActivity(), listPtm));
        lv.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_cmo_list_ptm);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent i = new Intent(getActivity(), ActivityViewCmoPtm.class);

        String ptmDate = listPtm.get(position).getPtmDate();
        String customerName = listPtm.get(position).getPtmCustomerName();
        String addressPtm = listPtm.get(position).getPtmAddress();
        String phoneNumber = listPtm.get(position).getPtmPhoneNumber();

        i.putExtra("varDatePTM", ptmDate);
        i.putExtra("varCustomerName", customerName);
        i.putExtra("varAddressPtm", addressPtm);
        i.putExtra("varPhoneNumber", phoneNumber);

        startActivity(i);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<ObjectPtm> getListPtm(){
        ArrayList<ObjectPtm> ptmlist = new ArrayList<ObjectPtm>();

        ObjectPtm ptm = new ObjectPtm();

        ptm.setPtmDate("10 November 2017");
        ptm.setPtmCustomerName("Christ Stuart");
        ptm.setPtmAddress("Jl Kebonsirih No12..");
        ptm.setPtmPhoneNumber("08112105811");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("13 November 2017");
        ptm.setPtmCustomerName("John Topher");
        ptm.setPtmAddress("Jl Bekasi Barat RT/..");
        ptm.setPtmPhoneNumber("08328363933");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("20 November 2017");
        ptm.setPtmCustomerName("Sammantha Angel");
        ptm.setPtmAddress("Jl Margonda raya KM..");
        ptm.setPtmPhoneNumber("085643762891");
        ptmlist.add(ptm);

        return ptmlist;
    }
}
