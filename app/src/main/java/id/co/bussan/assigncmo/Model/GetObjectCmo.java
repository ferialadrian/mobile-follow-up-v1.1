package id.co.bussan.assigncmo.Model;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import id.co.bussan.assigncmo.ObjectCmo;

/**
 * Created by 0426591017 on 11/13/2017.
 */

public class GetObjectCmo {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    List<ObjectCmo> listDataCmo;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ObjectCmo> getListDataCmo() {
        return listDataCmo;
    }

    public void setListDataCmo(List<ObjectCmo> listDataCmo) {
        this.listDataCmo = listDataCmo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
