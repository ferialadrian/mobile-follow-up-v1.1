package id.co.bussan.assigncmo;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityViewMhPtm extends AppCompatActivity implements
        FragmentAssignToCmo.OnFragmentInteractionListener,
        FragmentReassignBranch.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mh_ptm);
        getSupportActionBar().setTitle("PTM - MH Access");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView datePtm =  (TextView) findViewById(R.id.ptm_date);
        TextView customerName = (TextView) findViewById(R.id.ptm_customer_name);
        TextView addressPtm = (TextView) findViewById(R.id.ptm_address);
        TextView phoneNumber = (TextView) findViewById(R.id.ptm_phone_number);

        datePtm.setText(getIntent().getStringExtra("varDatePtm"));
        customerName.setText(getIntent().getStringExtra("varCustomerName"));
        addressPtm.setText(getIntent().getStringExtra("varAddressPtm"));
        phoneNumber.setText(getIntent().getStringExtra("varPhoneNumber"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    /*
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_view_mh_ptm);

            Button reassign_branch_btn = (Button) findViewById(R.id.reassign_branch);
            reassign_branch_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    Toast.makeText(ActivityViewMhPtm.this, "Anda Telah Melakukan REASSIGN BRANCH", Toast.LENGTH_SHORT).show();
*/

    public void assignCmo(View view){

        Button reassign_branch_btn=(Button)findViewById(R.id.reassign_branch);
        reassign_branch_btn.setVisibility(View.GONE);

        Fragment fragment = null;
        Class fragmentClass = null;

        fragmentClass = FragmentAssignToCmo.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_mh_cmo, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void reassignBranch(View view){
        Button apply_to_cmo_btn=(Button)findViewById(R.id.apply_to_cmo);
        apply_to_cmo_btn.setVisibility(View.GONE);

        Fragment fragment = null;
        Class fragmentClass = null;

        fragmentClass = FragmentReassignBranch.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_mh_cmo, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void detailptm(View view) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog_details, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Detail Informasi");
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }

        });

        AlertDialog dialog = alert.create();
        dialog.show();

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Button reassign_branch_btn=(Button)findViewById(R.id.reassign_branch);
        reassign_branch_btn.setVisibility(View.VISIBLE);

        Button apply_to_cmo_btn=(Button)findViewById(R.id.apply_to_cmo);
        apply_to_cmo_btn.setVisibility(View.VISIBLE);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
