package id.co.bussan.assigncmo;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;


public class FragmentReassignBranch extends Fragment implements AdapterView.OnItemClickListener, SearchView.OnQueryTextListener {

    AdapterCustomSearchBranch adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_reassign_branch, container,false);

        ArrayList<ObjectBranchNames> listBranch = getListBranch();
        ListView lv = (ListView)view.findViewById(R.id.list_branch_name);
        // Pass results to ListViewAdapter Class
        adapter = new AdapterCustomSearchBranch(getActivity(), listBranch);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        // Locate the EditText in listview_main.xml
        SearchView editsearch = (SearchView) view.findViewById(R.id.search);
        editsearch.setOnQueryTextListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getContext(), "anda telah melakukan assign ke cabang yang dipilih", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        String text = s;
        adapter.filter(text);
        return false;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<ObjectBranchNames> getListBranch(){
        String[] branchNameList;
        ArrayList<ObjectBranchNames> arraylist = new ArrayList<ObjectBranchNames>();

        branchNameList = new String[]{"Cabang 1", "Cabang 2", "Cabang 3",
                "Cabang 4", "Cabang 5", "Cabang 6", "Cabang 7", "Cabang 8",
                "Cabang 9","Cabang 10","Cabang 11"};

        for (int i = 0; i < branchNameList.length; i++) {
            ObjectBranchNames objectBranchNames = new ObjectBranchNames(branchNameList[i]);
            // Binds all strings into an array
            arraylist.add(objectBranchNames);
        }

        return arraylist;
    }
}
