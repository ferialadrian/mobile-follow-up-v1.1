package id.co.bussan.assigncmo.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by 0426591017 on 11/13/2017.
 */

public class ApiClient {
    // public static final String BASE_URL = "http://10.1.191.125/assigncmo/public/";
    // public static final String BASE_URL = "http://172.30.60.125/assigncmo/public/";
    public static final String BASE_URL = "http://172.30.60.196/mobilefu-api/public/";
    private static Retrofit retrofit = null;
    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
