package id.co.bussan.assigncmo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.co.bussan.assigncmo.Model.GetObjectCmo;
import id.co.bussan.assigncmo.Model.GetObjectEmployee;
import id.co.bussan.assigncmo.Rest.ApiInterface;
import id.co.bussan.assigncmo.Rest.InternetConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMain extends AppCompatActivity
{
    ApiInterface apiInterface;
    EditText editUsername, editPassword;

    Button loginBtn;

    String nik, password;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editUsername = (EditText) findViewById(R.id.lg_username);
        editPassword = (EditText) findViewById(R.id.lg_password);
        nik = editUsername.getText().toString();
        password = editPassword.getText().toString();

        loginBtn = (Button) findViewById(R.id.loginButton);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin(v);
            }
        });
    }

    public void doLogin(View view)
    {
        editUsername = (EditText)findViewById(R.id.lg_username);
        editPassword = (EditText)findViewById(R.id.lg_password);
        nik = editUsername.getText().toString();
        password = editPassword.getText().toString();

        if ((nik.contains("1")) && (password.contains("1")))
        {
            Toast.makeText(this, R.string.login_sukses, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(ActivityMain.this, ActivityHome.class);
            startActivity(i);
        }
        else if(nik.matches("") || password.matches("")){
            Toast.makeText(this, R.string.login_empty, Toast.LENGTH_SHORT).show();
        }
        else
            {
            Toast.makeText(this, R.string.login_gagal, Toast.LENGTH_SHORT).show();
        }
    }
//
//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        //In onresume fetching value from sharedpreference
//        SharedPreferences sharedPreferences = getSharedPreferences(ApplicationConfiguration.SHARED_PREF_NAME,Context.MODE_PRIVATE);
//
//        //Fetching the boolean value form sharedpreferences
//        loggedIn = sharedPreferences.getBoolean(ApplicationConfiguration.LOGGEDIN_SHARED_PREF, false);
//
//        //If we will get true
//        if(loggedIn)
//        {
//            //We will start the Profile Activity
//            Intent intent = new Intent(ActivityMain.this, ProfileActivity.class);
//            startActivity(intent);
//        }
//    }
//
//    private void login() {
//        //Getting values from edit texts
//        final String nik = editTextNik.getText().toString().trim();
//        final String password = editTextPassword.getText().toString().trim();
//
//        //Creating a string request
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApplicationConfiguration.LOGIN_URL,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        //If we are getting success from server
//                        if (response.equalsIgnoreCase(ApplicationConfiguration.LOGIN_SUCCESS)) {
//                            //Creating a shared preference
//                            SharedPreferences sharedPreferences = LoginActivity.this.getSharedPreferences(ApplicationConfiguration.SHARED_PREF_NAME, Context.MODE_PRIVATE);
//
//                            //Creating editor to store values to shared preferences
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//
//                            //Adding values to editor
//                            editor.putBoolean(ApplicationConfiguration.LOGGEDIN_SHARED_PREF, true);
//                            editor.putString(ApplicationConfiguration.NIK_SHARED_PREF, nik);
//
//                            //Saving values to editor
//                            editor.commit();
//
//                            //Starting profile activity
//                            Intent intent = new Intent(ActivityMain.this, ProfileActivity.class);
//                            startActivity(intent);
//                        } else {
//                            //If the server response is not success
//                            //Displaying an error message on toast
//                            Toast.makeText(ActivityMain.this, "Invalid username or password", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        //You can handle error here if you want
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                //Adding parameters to request
//                params.put(ApplicationConfiguration.KEY_NIK, nik);
//                params.put(ApplicationConfiguration.KEY_PASSWORD, password);
//
//                //returning parameter
//                return params;
//            }
//        }
//    }
}
