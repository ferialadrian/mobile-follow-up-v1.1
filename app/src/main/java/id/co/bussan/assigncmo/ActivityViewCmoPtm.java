package id.co.bussan.assigncmo;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class ActivityViewCmoPtm extends AppCompatActivity implements FragmentReasonNotApply.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cmo_ptm);
        getSupportActionBar().setTitle("PTM - CMO Access");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView datePTM = (TextView) findViewById(R.id.ptm_date);
        TextView customerName = (TextView) findViewById(R.id.ptm_customer_name);
        TextView addressPtm = (TextView) findViewById(R.id.ptm_address);
        TextView phoneNumber = (TextView) findViewById(R.id.ptm_phone_number);

        datePTM.setText(getIntent().getStringExtra("varDatePTM"));
        customerName.setText(getIntent().getStringExtra("varCustomerName"));
        addressPtm.setText(getIntent().getStringExtra("varAddressPtm"));
        phoneNumber.setText(getIntent().getStringExtra("varPhoneNumber"));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    /* by 0426611017 on 15/11/2017 */

    public void apply(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        //Pengaturan untuk Title Alert Dialog
        alertDialogBuilder.setTitle("Confirm to Apply");
        //Pengaturan pesan Alert Dialog
        alertDialogBuilder.setMessage("Apakah anda yakin untuk Apply ?");
        alertDialogBuilder.setCancelable(false);

        //Pengaturan Opsi Yes & No
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ActivityViewCmoPtm.this, "Anda memilih apply", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ActivityViewCmoPtm.this, "Anda memilih untuk tidak apply", Toast.LENGTH_SHORT).show();

            }
        });

        alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void showReason(View view) {
        Button apply_btn = (Button) findViewById(R.id.apply_btn);
        apply_btn.setVisibility(View.GONE);

        Button reschedule_btn = (Button) findViewById(R.id.reschedule_btn);
        reschedule_btn.setVisibility(View.GONE);

        Fragment fragment = null;
        Class fragmentClass = null;

        fragmentClass = FragmentReasonNotApply.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.reason_not_apply_frame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void reschedule(View view) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog_reschedule, null);
        final DatePicker DatePicker = alertLayout.findViewById(R.id.DatePicker);

        /*final Button submit = alertLayout.findViewById(R.id.button);

        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String day = "Tanggal = " + DatePicker.getDayOfMonth();
                String month = "Bulan = " + (DatePicker.getMonth() + 1);
                String year = "Tahun =" + DatePicker.getYear();
                Toast.makeText(getApplicationContext(),"OK", Toast.LENGTH_LONG).show();
            }
        });*/

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Pilih Tanggal Re-schedule");
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(), "Anda belum melakukan re-schedule", Toast.LENGTH_LONG).show();
            }

        });

        alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String day = "Tanggal = " + DatePicker.getDayOfMonth();
                String month = "Bulan = " + (DatePicker.getMonth() + 1);
                String year = "Tahun =" + DatePicker.getYear();
                Toast.makeText(getApplicationContext(), "Anda telah melakukan re-schedule di tanggal" + "\n" + "\n" + day + "\n" + month + "\n" + year, Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void detailptm(View view) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog_details, null);


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Detail Informasi");
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }

        });

        AlertDialog dialog = alert.create();
        dialog.show();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Button apply_btn=(Button)findViewById(R.id.apply_btn);
        apply_btn.setVisibility(View.VISIBLE);

        Button reschedule_btn=(Button)findViewById(R.id.reschedule_btn);
        reschedule_btn.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
