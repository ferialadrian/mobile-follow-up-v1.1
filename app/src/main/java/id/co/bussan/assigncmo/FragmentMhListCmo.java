package id.co.bussan.assigncmo;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import id.co.bussan.assigncmo.Model.GetObjectCmo;
import id.co.bussan.assigncmo.Rest.ApiClient;
import id.co.bussan.assigncmo.Rest.ApiInterface;
import id.co.bussan.assigncmo.Rest.InternetConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMhListCmo extends Fragment {
    ApiInterface apiInterface;
    TextView icon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_mh_list_cmo, container, false);

        ListView lv = (ListView)view.findViewById(R.id.list_view_cmo);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        refresh(lv);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_mh_list_cmo);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void refresh(final ListView view) {
        if (InternetConnection.checkConnection(getContext())) {
            Call<GetObjectCmo> cmoCall = apiInterface.getCmo();
            cmoCall.enqueue(new Callback<GetObjectCmo>() {
                @Override
                public void onResponse(Call<GetObjectCmo> call, Response<GetObjectCmo>
                        response) {
                    if (response.isSuccessful()) {
                        List<ObjectCmo> cmoList = response.body().getListDataCmo();
                        view.setAdapter(new AdapterCustomListCmo(getActivity(), cmoList));
                    } else {
                        Toast.makeText(getContext(), R.string.error_api, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetObjectCmo> call, Throwable t) {
                    Log.e("Retrofit Get", t.toString());
                }
            });
        } else {
            Toast.makeText(getContext(), R.string.no_connectivity, Toast.LENGTH_SHORT).show();

            List<ObjectCmo> cmoArrayList = new ArrayList<ObjectCmo>();
            ObjectCmo cmo = new ObjectCmo();
            cmo.setCmoName("Budi");
            cmo.setCmoCase("25");
            cmo.setCmoCaseUpdate("15 Case Update");
            cmo.setCmoCaseNotUpdate("10 Case Not Updated");
            cmoArrayList.add(cmo);

            cmo = new ObjectCmo();
            cmo.setCmoName("Hari");
            cmo.setCmoCase("25");
            cmo.setCmoCaseUpdate("15 Case Update");
            cmo.setCmoCaseNotUpdate("10 Case Not Updated");
            cmoArrayList.add(cmo);

            cmo = new ObjectCmo();
            cmo.setCmoName("Andrew");
            cmo.setCmoCase("25 Case");
            cmo.setCmoCaseUpdate("15 Case Update");
            cmo.setCmoCaseNotUpdate("10 Case Not Updated");
            cmoArrayList.add(cmo);

            cmo = new ObjectCmo();
            cmo.setCmoName("Andrew Garfield");
            cmo.setCmoCase("25 Case");
            cmo.setCmoCaseUpdate("15 Case Update");
            cmo.setCmoCaseNotUpdate("10 Case Not Updated");
            cmoArrayList.add(cmo);

            view.setAdapter(new AdapterCustomListCmo(getActivity(), cmoArrayList));
        }
    }
}
