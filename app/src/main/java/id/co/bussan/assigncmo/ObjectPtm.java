package id.co.bussan.assigncmo;

/**
 * Created by 0426591017 on 11/3/2017.
 */

public class ObjectPtm
{

    private String ptmDate;
    private String ptmCustomerName;
    private String ptmAddress;
    private String ptmPhoneNumber;

    public String getPtmDate() {
        return ptmDate;
    }

    public void setPtmDate(String ptmDate)
    {
        this.ptmDate = ptmDate;
    }



    public String getPtmCustomerName() {
        return ptmCustomerName;
    }

    public void setPtmCustomerName(String ptmCustomerName)
    {
        this.ptmCustomerName = ptmCustomerName;
    }



    public String getPtmAddress() {
        return ptmAddress;
    }

    public void setPtmAddress(String ptmAddress)
    {
        this.ptmAddress = ptmAddress;
    }



    public String getPtmPhoneNumber() {
        return ptmPhoneNumber;
    }

    public void setPtmPhoneNumber(String ptmPhoneNumber)
    {
        this.ptmPhoneNumber = ptmPhoneNumber;
    }
}
