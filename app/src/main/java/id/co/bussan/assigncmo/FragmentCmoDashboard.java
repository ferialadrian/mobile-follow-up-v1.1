package id.co.bussan.assigncmo;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class FragmentCmoDashboard extends Fragment {

    TextView icon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cmo_dashboard, container, false);
        ArrayList<ObjectPtm> listPtm = getListPtm();
        ListView lv = (ListView)view.findViewById(R.id.list_view_dash_cmo_ptm);
        lv.setAdapter(new AdapterCustomListPtm(getActivity(), listPtm));

        icon = (TextView) view.findViewById(R.id.notif_icon);

        Animation animBlinkIn_icon = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.blink_notification);
        icon.startAnimation(animBlinkIn_icon);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_cmo_dashboard);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<ObjectPtm> getListPtm(){
        ArrayList<ObjectPtm> ptmlist = new ArrayList<ObjectPtm>();

        ObjectPtm ptm = new ObjectPtm();

        ptm.setPtmDate("10 November 2017");
        ptm.setPtmCustomerName("Christ Stuart");
        ptm.setPtmAddress("Jl Kebonsirih No12..");
        ptm.setPtmPhoneNumber("08112105811");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("13 November 2017");
        ptm.setPtmCustomerName("John Topher");
        ptm.setPtmAddress("Jl Bekasi Barat RT/..");
        ptm.setPtmPhoneNumber("08328363933");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("20 November 2017");
        ptm.setPtmCustomerName("Samantha Angel");
        ptm.setPtmAddress("Jl Margonda raya KM..");
        ptm.setPtmPhoneNumber("085643762891");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("21 November 2017");
        ptm.setPtmCustomerName("Joe Taslim");
        ptm.setPtmAddress("Perumahan Buah Batu - Bandung");
        ptm.setPtmPhoneNumber("08127823231");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("22 November 2017");
        ptm.setPtmCustomerName("Oliver W.");
        ptm.setPtmAddress("Tanah Baru Residence - Depok");
        ptm.setPtmPhoneNumber("085787365281");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("23 November 2017");
        ptm.setPtmCustomerName("Komar");
        ptm.setPtmAddress("Jl. Margasatwa 1 No. 12 - Ciledug");
        ptm.setPtmPhoneNumber("08781235678");
        ptmlist.add(ptm);

        ptm = new ObjectPtm();
        ptm.setPtmDate("24 November 2017");
        ptm.setPtmCustomerName("Kevin A.");
        ptm.setPtmAddress("Jl. Melawai 2 No.54a - Jakarta");
        ptm.setPtmPhoneNumber("08531232131");
        ptmlist.add(ptm);

        return ptmlist;
    }

}
