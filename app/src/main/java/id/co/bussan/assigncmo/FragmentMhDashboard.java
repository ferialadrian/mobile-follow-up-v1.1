package id.co.bussan.assigncmo;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class FragmentMhDashboard extends Fragment {

    TextView icon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mh_dashboard, container, false);

        icon = (TextView) view.findViewById(R.id.notif_icon);

        //notification icon
        Animation animBlinkIn_icon = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.blink_notification);
        icon.startAnimation(animBlinkIn_icon);


    return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.menu_mh_dashboard);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }




}
