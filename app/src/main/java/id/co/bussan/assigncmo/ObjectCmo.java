package id.co.bussan.assigncmo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 11/2/2017.
 */

public class ObjectCmo {

    @SerializedName("name")
    private String cmoName;

    @SerializedName("total_case")
    private String cmoCase;

    @SerializedName("update_case")
    private String cmoCaseUpdate;

    @SerializedName("not_update_case")
    private String cmoCaseNotUpdate;

    public String getCmoName() {
        return cmoName;
    }

    public void setCmoName(String cmoName) {
        this.cmoName = cmoName;
    }

    public String getCmoCase() {
        return cmoCase;
    }

    public void setCmoCase(String cmoCase) {
        this.cmoCase = cmoCase;
    }

    public String getCmoCaseUpdate() {
        return cmoCaseUpdate;
    }

    public void setCmoCaseUpdate(String cmoCaseUpdate) {
        this.cmoCaseUpdate = cmoCaseUpdate;
    }

    public String getCmoCaseNotUpdate() {
        return cmoCaseNotUpdate;
    }

    public void setCmoCaseNotUpdate(String cmoCaseNotUpdate) {
        this.cmoCaseNotUpdate = cmoCaseNotUpdate;
    }
}
