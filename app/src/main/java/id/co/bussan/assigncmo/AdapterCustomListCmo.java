package id.co.bussan.assigncmo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 0426591017 on 11/2/2017.
 */

public class AdapterCustomListCmo extends BaseAdapter {
    private static List<ObjectCmo> listCmo;
    private LayoutInflater mInflater;

    public AdapterCustomListCmo(Context mhListCmoFragment, List<ObjectCmo> results){
        listCmo = results;
        mInflater = LayoutInflater.from(mhListCmoFragment);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.listview_cmo_row, null);
            holder = new ViewHolder();
            holder.cmoNameTextField = (TextView) convertView.findViewById(R.id.cmo_name);
            holder.cmoCaseTextField = (TextView) convertView.findViewById(R.id.cmo_case);
            holder.cmoCaseUpdateTextField = (TextView) convertView.findViewById(R.id.cmo_case_update);
            holder.cmoCaseNotUpdateTextField = (TextView) convertView.findViewById(R.id.cmo_case_not_update);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String cmoCase = listCmo.get(position).getCmoCase();
        String labelCmoCase = convertView.getResources().getString(R.string.label_case_cmo);
        String cmoCaseUpdate = listCmo.get(position).getCmoCaseUpdate();
        String labelCmoCaseUpdate = convertView.getResources().getString(R.string.label_case_update_cmo);
        String cmoCaseNotUpdate = listCmo.get(position).getCmoCaseNotUpdate();
        String labelCmoCaseNotUpdate = convertView.getResources().getString(R.string.label_case_not_update_cmo);

        holder.cmoNameTextField.setText(listCmo.get(position).getCmoName());
        holder.cmoCaseTextField.setText(cmoCase + " " + labelCmoCase);
        holder.cmoCaseUpdateTextField.setText(cmoCaseUpdate + " " + labelCmoCaseUpdate);
        holder.cmoCaseNotUpdateTextField.setText(cmoCaseNotUpdate + " " + labelCmoCaseNotUpdate);

        return convertView;
    }

    static class ViewHolder{
        TextView cmoNameTextField;
        TextView cmoCaseTextField;
        TextView cmoCaseUpdateTextField;
        TextView cmoCaseNotUpdateTextField;
    }

    @Override
    public int getCount() {
        return listCmo.size();
    }

    @Override
    public Object getItem(int arg0) {
        return listCmo.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
}
