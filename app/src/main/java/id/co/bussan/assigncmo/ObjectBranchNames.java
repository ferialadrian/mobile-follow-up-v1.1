package id.co.bussan.assigncmo;

public class ObjectBranchNames {
    private String branchName;

    public ObjectBranchNames(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchName() {
        return this.branchName;
    }

}
