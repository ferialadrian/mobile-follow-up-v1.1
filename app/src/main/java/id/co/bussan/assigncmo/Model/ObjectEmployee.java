package id.co.bussan.assigncmo.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 0427701017 on 11/22/2017.
 */

public class ObjectEmployee {

    @SerializedName("employeeID")
    private String employeeID;

    @SerializedName("nik")
    private String employeeNik;

    @SerializedName("employeeName")
    private String employeeName;

    @SerializedName("email")
    private String employeeEmail;

    @SerializedName("phoneNumber")
    private String phoneNumber;

    @SerializedName("branchName")
    private String branchName;

    @SerializedName("isActive")
    private String isActive;

    @SerializedName("noIMEI")
    private String noIMEI;

    @SerializedName("createDate")
    private String createDate;

    @SerializedName("financeCode")
    private String financeCode;

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getEmployeeNik() {
        return employeeNik;
    }

    public void setEmployeeNik(String employeeNik) {
        this.employeeNik = employeeNik;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getNoIMEI() {
        return noIMEI;
    }

    public void setNoIMEI(String noIMEI) {
        this.noIMEI = noIMEI;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getFinanceCode() {
        return financeCode;
    }

    public void setFinanceCode(String financeCode) {
        this.financeCode = financeCode;
    }
}
