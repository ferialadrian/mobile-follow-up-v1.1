package id.co.bussan.assigncmo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterCustomSearchBranch extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private List<ObjectBranchNames> objectBranchNamesList = null;
    private ArrayList<ObjectBranchNames> arraylist;

    public AdapterCustomSearchBranch(Context context, List<ObjectBranchNames> objectBranchNamesList) {
        mContext = context;
        this.objectBranchNamesList = objectBranchNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<ObjectBranchNames>();
        this.arraylist.addAll(objectBranchNamesList);
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return objectBranchNamesList.size();
    }

    @Override
    public ObjectBranchNames getItem(int position) {
        return objectBranchNamesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_view_items, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(objectBranchNamesList.get(position).getBranchName());
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        objectBranchNamesList.clear();
        if (charText.length() == 0) {
            objectBranchNamesList.addAll(arraylist);
        } else {
            for (ObjectBranchNames wp : arraylist) {
                if (wp.getBranchName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    objectBranchNamesList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}