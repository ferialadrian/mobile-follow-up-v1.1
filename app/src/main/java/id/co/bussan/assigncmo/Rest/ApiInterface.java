package id.co.bussan.assigncmo.Rest;

import id.co.bussan.assigncmo.Model.GetObjectCmo;
import id.co.bussan.assigncmo.Model.GetObjectEmployee;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by 0426591017 on 11/13/2017.
 */

public interface  ApiInterface
{
    @GET("api/employee")
    Call<GetObjectCmo> getCmo();

    @GET("api/main/login")
    Call<GetObjectEmployee> getEmployee();

}
