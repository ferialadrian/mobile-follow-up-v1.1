package id.co.bussan.assigncmo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class FragmentAssignToCmo extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_assign_to_cmo, container, false);

        ArrayList<ObjectCmo> listCmo = getListCmo();
        ListView lv = (ListView)view.findViewById(R.id.list_view_assign_cmo);
        lv.setAdapter(new AdapterCustomListAssignToCmo(getActivity(), listCmo));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<ObjectCmo> getListCmo(){
        ArrayList<ObjectCmo> cmolist = new ArrayList<ObjectCmo>();

        ObjectCmo cmo = new ObjectCmo();

        cmo.setCmoName("Adi");
        cmo.setCmoCase("25 Case");
        cmolist.add(cmo);

        cmo = new ObjectCmo();
        cmo.setCmoName("Budi");
        cmo.setCmoCase("56 Case");
        cmolist.add(cmo);

        cmo = new ObjectCmo();
        cmo.setCmoName("Doni");
        cmo.setCmoCase("22 Case");
        cmolist.add(cmo);

        cmo = new ObjectCmo();
        cmo.setCmoName("Maman");
        cmo.setCmoCase("29 Case");
        cmolist.add(cmo);

        return cmolist;
    }
}